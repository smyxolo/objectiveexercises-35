package Zadanie19_Family;

public class Mother extends FamilyMember{
    public Mother(String name) {
        super(name);
    }

    @Override
    public boolean isAdult() {
        return true;
    }
}
