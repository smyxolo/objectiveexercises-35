package Zadanie19_Family;

public class Daughter extends FamilyMember {
    public Daughter(String name) {
        super(name);
    }

    @Override
    public boolean isAdult() {
        return false;
    }
}
