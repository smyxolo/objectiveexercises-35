package Zadanie19_Family;

public class Son extends FamilyMember {
    public Son(String name) {
        super(name);
    }

    @Override
    public boolean isAdult() {
        return false;
    }
}
