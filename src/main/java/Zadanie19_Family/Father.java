package Zadanie19_Family;

public class Father extends FamilyMember {
    public Father(String name) {
        super(name);
    }

    @Override
    public boolean isAdult() {
        return true;
    }
}
