package Zadanie35_CustomerServiceModel;

import java.util.HashMap;

public class Office {

private String city;
private HashMap<Integer, Client> mapaKlientow = new HashMap<>();

    public Office(String city) {
        this.city = city;
    }

    public void handle(Client klient){
        System.out.println("Client PESEL: " + klient.getPESEL() +  " is handling " + klient.getTyp() + " case.");
    }

    public void addClient(int pesel, typSprawy typ){
        mapaKlientow.put(pesel, new Client(typ, pesel));
    }
    public Client getClient(int pesel){
        return mapaKlientow.get(pesel);
    }

    public HashMap<Integer, Client> getMapaKlientow() {
        return mapaKlientow;
    }

    public String getCity() {
        return city;
    }
}
