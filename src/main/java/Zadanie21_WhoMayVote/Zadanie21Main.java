package Zadanie21_WhoMayVote;

public class Zadanie21Main {
    public static void main(String[] args) {
        Town gdansk = new Town();
        gdansk.addCitizen(new Peasant("Grzegorz"));
        gdansk.addCitizen(new King("Edward"));
        gdansk.addCitizen(new TownsMan("Krzysztof"));
        gdansk.addCitizen(new Peasant("Asia"));
        gdansk.addCitizen(new Soldier("Antoni"));
        gdansk.addCitizen(new Soldier("Ryan"));
        gdansk.addCitizen(new Peasant("Jerzy"));

        System.out.println(gdansk.howManyCanVote() + " obywateli moze glosowac.");
        System.out.println("Obywatele glosujacy to: " + gdansk.whoCanVote());
    }
}
