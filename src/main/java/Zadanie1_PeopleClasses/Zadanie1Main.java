package Zadanie1_PeopleClasses;

public class Zadanie1Main {
    public static void main(String[] args) {
        Person person1 = new Person("Tom", 3);
        Person person2 = new Person("Jane", 9);
        Person person3 = new Person("George", 29);
        person1.printNameAndAge();
        person2.printNameAndAge();
        person3.printNameAndAge();

        System.out.println(person1);
        System.out.println(person2);
        System.out.println(person3);

        person1.setName("Grace");
        System.out.println("I am " + person1.getName() + " and I am " + person1.getAge());
    }
}
