package Zadanie6_QuadraticEquation;

import java.util.Scanner;

public class Zadanie6Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        System.out.println("Podaj liczby a, b, c dla rownania w formacie:");
        System.out.println("ax^2  + bx + c");
        String linia = skan.nextLine();

        try {
            double a = Double.parseDouble(linia.split(" ", 3)[0]);
            double b = Double.parseDouble(linia.split(" ", 3)[1]);
            double c = Double.parseDouble(linia.split(" ", 3)[2]);

            QuadraticEquation quadraticEquation = new QuadraticEquation();
            quadraticEquation.setA(a);
            quadraticEquation.setB(b);
            quadraticEquation.setC(c);

            System.out.println("Delta = " + quadraticEquation.calculateDelta());
            if(quadraticEquation.calculateDelta() > 0){
                System.out.println("Pierwiastki rownania " + a + " * x^2 + " + b + "x + " + c + " = 0:");
                System.out.println("X1 = " + quadraticEquation.calculateX1());
                System.out.println("X2 = " + quadraticEquation.calculateX2());
            }
            else if(quadraticEquation.calculateDelta() == 0){
                System.out.println("Rownanie "+ a + " * x^2 + " + b + "x + " + c + " = 0 ma jedno rozwiazanie:");
                System.out.println("X =" + quadraticEquation.calculateX1());
            }
            else {
                System.out.println("Rownanie nie ma rozwiazan.");
            }

        }
        catch (NumberFormatException nfe){
            System.out.println("Nieprawidlowy format liczb");
        }

    }
}
