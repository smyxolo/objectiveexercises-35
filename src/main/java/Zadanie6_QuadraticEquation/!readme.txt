Zadanie 6:
    http://matematyka.pisz.pl/strona/54.html

    Napisz klasę QuadraticEquation (obliczanie równania kwadratowego) o trzech polach typu double a,b,c odpowiednim konstruktorze i metodach:
        - public double calculateDelta(),
        - public double calculateX1(),
        - public double calculateX2().
    Wartości trzech pól powinny być ustawiane w konstruktorze.
    Metody powinny obliczać wartości na podstawie wartości pól obiektu, a następnie zwracać wartości.
    Do obliczenia pierwiastka użyj:
        double pierwiastek = Math.sqrt(jakasWartosc);