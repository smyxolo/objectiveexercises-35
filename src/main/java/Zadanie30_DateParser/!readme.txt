Zadanie 30 (Time):
    Stwórz aplikacje która ze scannera przyjmuje datę w formacie '2013-03-20 12:40' (ROK-MSC-DZIEN GODZINA:DATA)
    i wypisuje komunikat ze data jest w poprawnym formacie lub (jeśli nie jest) rzuca Exception "WrongDateTimeException" który jest wyjątkiem,
     który dziedziczy po RuntimeException (napisz go sam/a).

    Aplikacja w pętli ma czytać z wejścia (Scanner'a) linię i:
        - jeśli wpisano quit to program ma konczyc prace
        - jesli wpisano date, to podejmij probe parsowania daty. Jesli :
                        a. data jest w poprawnym formacie to wypisz komunikat "data w poprawnym formacie"
                        b. data jest w niepoprawnym formacie to wypisz odpowiedni komunikat

    dodatkowe.** Zmodyfikuj aplikację lub utwórz podobną aplikację,
    która na początku prosi o format daty (np. yyyy-mm-dd HH:mm) a po podaniu formatu daty
                działa tak jak poprzednia aplikacja, czyli przyjmuje datę w podanym formacie (tym wpisanym na początku aplikacji).