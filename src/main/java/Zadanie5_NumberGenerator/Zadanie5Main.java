package Zadanie5_NumberGenerator;

public class Zadanie5Main {

public static void main(String[] args) {
    Generator generator = new Generator(0,10,20,false);
    System.out.println(generator);
    System.out.println(generator.generateNextFlag());
    System.out.println(generator.generateNumber());
    System.out.println(generator.generateInRange());
    System.out.println(generator.generateNextEvenNumber());
    System.out.println(generator.generateNextMultiple(5));
    System.out.println(generator.generateNextChar('a'));

}
}
