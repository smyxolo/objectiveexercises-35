package Zadanie9_MyMath;

public class MyMath {
    public int abs(int a){
        return Math.abs(a);
    }
    public double abs(double a){
        return Math.abs(a);
    }
    public int pow(int a, int b){
        return (int) (Math.pow((double)a, (double)b));
    }
    public double pow(double a, double b){
        return Math.pow(a, b);
    }
}
