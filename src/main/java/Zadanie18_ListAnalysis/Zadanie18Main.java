package Zadanie18_ListAnalysis;

import java.util.ArrayList;

public class Zadanie18Main {
    public static void main(String[] args) {

        ArrayList<Integer> lista = new ArrayList<>();

        lista.add(5);
        lista.add(7);
        lista.add(9);

        System.out.println("Sum: " + calculateSum(lista));
        System.out.println("Product: " + calculateProduct(lista));
        System.out.println("Average: " + calculateAverage(lista));

        ArrayList<User> userList = new ArrayList<>();

        User u = new User("Tomasz", "asdfasdfas");
        userList.add(u); // przekazanie referencji (normalne)
        userList.add(new User("Grzegorz", "akskdieif")); //przekazanie tzw. anonimowe

        System.out.println(u.toString());
        System.out.println(userList.get(0).toString());

        System.out.println(userList);

        int[] tablica = new int[10];

        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = (int)(Math.random()*10);
            System.out.print(tablica[i] + " ");

        }

        ArrayList<Integer> listaIntow = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            listaIntow.add((int)(Math.random()*10));
        }
        System.out.println("\n" + listaIntow);

        System.out.println();
        System.out.println("Rownych wartosci na tych samych indeksach: " + checkHowManyEqual(tablica, listaIntow));

        //skopiuj liste
        ArrayList<Integer> listaIntow2 = new ArrayList<>();
        for (Integer value: listaIntow) {
            listaIntow2.add(value);
        }

        System.out.println(listaIntow2);

        //przepisz liste do tablicy
        int rozmiar = listaIntow2.size();
        int[] tablica2 = new int[rozmiar];
        for (int i = 0; i < rozmiar; i++) {
            tablica2[i] = listaIntow2.get(i);
            System.out.print(tablica2[i] + " ");
        }

        //przepisz tablice do listy
        ArrayList<Integer> listaIntow3 = new ArrayList<>();
        for (Integer wartosc: tablica2) {
            listaIntow3.add(wartosc);
        }
        System.out.println("\n" + listaIntow3);

        //przepisz dwie listy do jednej

        // dopisac argumenty w nieokreslonej liczbie
        ArrayList<ArrayList<Integer>> listaList = new ArrayList<>();
        listaList.add(listaIntow);
        listaList.add(listaIntow2);
        listaList.add(listaIntow3);
        ArrayList<Integer> listaIntow4 = new ArrayList<>();
        for (ArrayList<Integer> aktualnaLista: listaList) {
            for (Integer wartosc1: aktualnaLista) {
                listaIntow4.add(wartosc1);
            }
        }
        System.out.println(listaIntow4);

        ArrayList<Integer> zupelnieNowaListaIntowZWieluListIntow = dodajListyIntow(listaIntow, listaIntow2, listaIntow3);
        System.out.println("Ponizej z nowejSuperFunkcji: ");
        System.out.println(zupelnieNowaListaIntowZWieluListIntow);

        System.out.println();

        System.out.println("Two lists are equal: " + areEqual(listaIntow, zupelnieNowaListaIntowZWieluListIntow));
        System.out.println("Two other lists are equal: " + areEqual(listaIntow2, listaIntow3));


    }

    private static ArrayList<Integer> dodajListyIntow(ArrayList<Integer>... listyDoDodania){
        ArrayList<Integer> nowalista = new ArrayList<>();
        for (int i = 0; i < listyDoDodania.length; i++) {
            for (Integer wartosc: listyDoDodania[i]) {
                nowalista.add(wartosc);
            }
        }
        return nowalista;
    }

    private static boolean areEqual(ArrayList<Integer> lista1, ArrayList<Integer> lista2){
        boolean takiesame = true;
        //porownajrozmiar
        if (lista1.size() == lista2.size()) {
            //jesli taki sam to porownaj kazdy element o tym samym indeksie
            for (int i = 0; i < lista1.size(); i++) {
                if(lista1.get(i) != lista2.get(i)){
                    takiesame = false;
                }
            }
        }
        //jesli inny rozmiar to rozne listy
        else {
            takiesame = false;
        }
        return takiesame;
    }

    //niewiemczydobrze
    private static ArrayList<Integer> copyIntegerList(ArrayList<Integer> jakasListaIntow){
        ArrayList<Integer> kopiaJakiejsListyIntow = jakasListaIntow;
        return kopiaJakiejsListyIntow;
    }

    private static int checkHowManyEqual(int[] jakasTablica, ArrayList<Integer> jakasLista){
        int common = 0;
        for (int i = 0; i < 10; i++) {
            if(jakasTablica[i] == jakasLista.get(i)){
                common++;
            }
        }
        return common;
    }

    private static Integer calculateAverage(ArrayList<Integer> jakaslista) {
        Integer average = calculateSum(jakaslista) / jakaslista.size();
        return average;
    }

    private static Integer calculateProduct(ArrayList<Integer> jakaslista) {
        Integer iloczyn = 1;
        for (Integer wartosc : jakaslista) {
            iloczyn *= wartosc;
        }
        return iloczyn;
    }

    private static Integer calculateSum(ArrayList<Integer> jakaslista) {
        Integer suma = 0;

        for (Integer wartosc: jakaslista) {
            suma += wartosc;

        }
        return suma;
    }

    // jeszcze metody na operacje logiczne
}
