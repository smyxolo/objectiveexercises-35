package Zadanie12_GeometryExpanded;

public abstract class Figure {
    public abstract double calculatePerimeter();
    public abstract double calculateArea();
}
