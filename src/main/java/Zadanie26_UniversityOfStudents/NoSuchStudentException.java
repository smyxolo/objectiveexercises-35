package Zadanie26_UniversityOfStudents;

public class NoSuchStudentException extends RuntimeException {

    public NoSuchStudentException(Long indexNumber){
        super("Nie ma takiego studenta co by mial indeks" + indexNumber);
    }
}
