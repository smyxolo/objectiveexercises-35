package Zadanie26_UniversityOfStudents;

public interface IStudentFormat {
    String format (Student student);
}
