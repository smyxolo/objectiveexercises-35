package Zadanie22_Wojna;

import Zadanie22_Wojna.GameElements.*;

import java.time.LocalDateTime;
import java.util.Random;

public class Game {
    private Random random = new Random();
    private GameResults results = new GameResults();
    private Deck deck = new Deck();
    private Player player1;
    private Player player2;
    private Table turnTable = new Table();

    public Game(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    public void play() {
        Player winner;

        // staraj sie nie stosowac dostepu do pól klasy.
        // powinna istniec metoda w obiekcie results
        // o np. nazwie startGame() - ktora ustawia czas startu i odpowiada za przygotowanie innych rzeczy (zalezy czy cos jeszcze jest do przygotowania)
        results.start = LocalDateTime.now();
        dealCardsToPlayers();

        while (bothPlayersHaveCards()) {
            makeTurn(false, turnTable);
            setCardValuesToCompare(turnTable);

            if (!war()) {
                winner = getStageWinner();
                shuffleReturnToWinner(winner, turnTable);
                // np. metoda nextRound()
                results.stageCount++;
            } else {
                while (war() && bothPlayersHaveCards()) {
                    try {
                        makeTurn(true, turnTable);
                    } catch (PlayerOutOfCardsException pooce) {
                        results.stageCount++;
                        break;
                    }
                    setCardValuesToCompare(turnTable);
                    results.stageCount++;
                }
                results.warCount++;
                winner = getStageWinner();
                shuffleReturnToWinner(winner, turnTable);
            }
        }
        results.finish = LocalDateTime.now();
        results.updateDuration();
        winner = getGameWinner(player1, player2, results);
        shuffleBackToDeck(winner, deck);
    }

    private void dealCardsToPlayers() {
        while (deck.cardList.size() > 0) {
            int randomIndex = random.nextInt(deck.cardList.size());
            player1.playerCardList.add(deck.cardList.get(randomIndex));
            // metoda remove (z listy) również zwraca Ci obiekt ktory usuwa. jesli zrobisz 
            // player1.playerCardList.add(deck.cardList.remove(randomIndex)); 
            // to to samo co pobranie i usuniecie ale znacznie szybciej
            deck.cardList.remove(randomIndex);
            randomIndex = random.nextInt(deck.cardList.size());
            player2.playerCardList.add(deck.cardList.get(randomIndex));
            deck.cardList.remove(randomIndex);
        }
    }

    private boolean bothPlayersHaveCards() {
        return player1.playerCardList.size() > 0 && player2.playerCardList.size() > 0;
    }

    private boolean war() { // staraj sie bardziej deskryptywnie opisywac metody. Metody zwracajace boolean powinny byc czytane jako pytanie, np. isWar - jest lub nie ?
        return player1.currentCardValue == player2.currentCardValue;
    }

    private void makeTurn(boolean war, Table turnTable) {
        if (war) {
            // poloz po dwie karty ze stosow kazdego gracza na ich stosy na stole
            for (int i = 1; i <= 4; i++) {
                if (bothPlayersHaveCards()) {
                    if (i % 2 == 0) {
                    // to samo co wyzej z remove'em
                        turnTable.player1Stack.add(0, player1.playerCardList.get(0));
                        player1.playerCardList.remove(0);
                    } else {
                        turnTable.player2Stack.add(0, player2.playerCardList.get(0));
                        player2.playerCardList.remove(0);
                    }

                } else throw new PlayerOutOfCardsException();
            }
            //jezeli zwykla tura
        } else if (bothPlayersHaveCards()) {
            // poloz po jednej karcie ze stosow graczy na ich stosy na stole
            turnTable.player1Stack.add(0, player1.playerCardList.get(0));
            player1.playerCardList.remove(0);
            turnTable.player2Stack.add(0, player2.playerCardList.get(0));
            player2.playerCardList.remove(0);
        } else throw new PlayerOutOfCardsException();
    }

    private void setCardValuesToCompare(Table turnTable) {
        player1.currentCardValue = turnTable.player1Stack.get(0).getValue();
        player2.currentCardValue = turnTable.player2Stack.get(0).getValue();
    }

    private Player getStageWinner() {
        if (player1.currentCardValue > player2.currentCardValue) return player1;
        else return player2;
    }

    private void shuffleReturnToWinner(Player winner, Table turnTable) {
        turnTable.consolidateCards();
        while (!turnTable.tableCards.isEmpty()) {
            int randomCardIndex = random.nextInt(turnTable.tableCards.size());
            winner.playerCardList.add(turnTable.tableCards.get(randomCardIndex));
            turnTable.tableCards.remove(randomCardIndex);
        }
    }

    private void shuffleBackToDeck(Player winner, Deck deck) {
        while (!winner.playerCardList.isEmpty()) {
            int randomCardIndex = random.nextInt(winner.playerCardList.size());
            deck.getCardList().add(winner.playerCardList.get(randomCardIndex));
            winner.playerCardList.remove(randomCardIndex);
        }
    }

    private Player getGameWinner(Player player1, Player player2, GameResults results) {
        if (player1.playerCardList.size() == 0) {
            // powinna istniec metoda ktora ustawia wybranego playera jako zwyciezce i poslugujac sie metoda w playerze powinienes ustawiac mu gamesWon ++
            // np. metoda player2.won()
            // drugi natomiast np. player1.lost()
            player2.gamesWon++;
            results.setWinner(player2);
            return player2;
        } else {
            player1.gamesWon++;
            results.setWinner(player1);
            return player1;
        }
    }

    public void playersSwitchPlaces() {
        Player tempPlayer = player1;
        player1 = player2;
        player2 = tempPlayer;
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public GameResults getResults() {
        return results;
    }

    public Deck getDeck() {
        return deck;
    }
}
