Zadanie 32:
    Stwórz aplikację która na:
        - wpisanie date wypisuje obecny LocalDate
        - wpisanie time wypisuje obecny LocalTime
        - wpisanie datetime wypisuje obecny LocalDateTime
        (W wybranym przez Ciebie formacie)
    Jeśli użytkownik wpisze 'quit' to zakoncz program.