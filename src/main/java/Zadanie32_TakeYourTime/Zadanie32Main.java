package Zadanie32_TakeYourTime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Zadanie32Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        String inputLine;
        DateTimeFormatter dater = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        DateTimeFormatter timer = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");
        DateTimeFormatter datimer = DateTimeFormatter.ofPattern("dd.MM.yyyy, HH:mm:ss");

        do{
            System.out.println("Wpisz date, time, datetime lub quit");
            inputLine = skan.nextLine();

            if(inputLine.equals("quit")){
                break;
            }
            else if(inputLine.equals("date")){
                System.out.println(LocalDate.now().format(dater));
            }
            else if (inputLine.equals("time")){
                System.out.println(LocalTime.now().format(timer));
            }
            else if (inputLine.equals("datetime")){
                System.out.println(LocalDateTime.now().format(datimer));
            }
            else {
                System.out.println("Nieprawidlowa komenda");
            }
        }
        while (!inputLine.equals("quit"));
    }
}
