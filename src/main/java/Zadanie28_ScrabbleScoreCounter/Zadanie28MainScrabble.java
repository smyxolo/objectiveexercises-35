package Zadanie28_ScrabbleScoreCounter;

import java.util.*;

public class Zadanie28MainScrabble {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        String inputLine = "";



        HashMap<Character, Integer> mapaWartosci = new HashMap<>();

        //przypisz wartosc 0 wszystkim znakom
        for (int i = 0; i < 128; i++) {
            mapaWartosci.put((char)i, 0);
        }

        //przypisz wszystkim literom amerykanskiego alfabetu "1"
        for (int i = 97; i < 123; i++) {
            mapaWartosci.put((char)i, 1);
        }
        
        //przypisz wartosci dla znakow o wyzszych wartosciach
        HashSet<Character> zbiorDwojek = new HashSet<>(Arrays.asList('y', 'h', 'q', 'x'));
        HashSet<Character> zbiorTrojek = new HashSet<>(Arrays.asList('ą', 'ę', 'ó', 'ż', 'ź', 'ć', 'ś'));
        for (Character znak: zbiorDwojek){
            mapaWartosci.put(znak,2);
        }
        for (Character znak :zbiorTrojek) {
            mapaWartosci.put(znak, 3);
        }
        
        //pobierz slowo
        System.out.println("Wprowadź ciąg znaków: ");
        inputLine = skan.nextLine();

        System.out.println("Wartość podanego ciągu to: " + rank(inputLine, mapaWartosci));
        
    }
    public static int rank(String input, HashMap<Character, Integer> mapaWartosci){
        int rank = 0;
        for (Character c : input.toLowerCase().toCharArray()) {
            rank += mapaWartosci.get(c);
        }
        return rank;
    }
}
