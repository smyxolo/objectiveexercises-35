amen [6:47 PM]
added this Plain Text snippet
Zadanie 28 (w domku):
  a. Napisz program tworzący mapę, która przyporządkowuje każdej liczbie z zakresu od 1 do 30 informację o tym, czy liczba jest liczbą pierwszą.
  b. Wypisz wszystkie pary na ekran w pętli for (entrySet)
  c. Wypisz wartości zwrócone z mapy dla kluczy 1,4,5,7,9,12
  d. Napisz metodę, która zwraca wartość słowa. Użyj mapy, która przyporządkowuje każdej literze wartość. Domyślna wartość dla litery to 1, dla pozostałych znaków 0.

  Poniższe znaki mają określone wartości:
    y,h,q,x - 2
    ą,ę,ó,ż,ź,ć,ś - 3

  sygnatura metody:

    public static int rank(String text);
      Przykłady:
        “ręka” -> 6
        “  .;a” -> 1