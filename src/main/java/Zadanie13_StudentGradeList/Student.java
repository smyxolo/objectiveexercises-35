package Zadanie13_StudentGradeList;

public class Student {
    private int indexNumber = 10496865;
    private String name = "Marek";
    private String surname = "Dybusc";
    private double[] marks = new double[100];
    private int markAmount = 0;
    private double sum = 0;

    public void addMark (double mark){
        if(markAmount < 100){
            marks[markAmount] = mark;
            sum += mark;
            markAmount++;
        }
        else {
            System.out.println("There is no more space in the registry.");
        }
    }


    public double calculateMarkAverage(){
        double average = sum / markAmount;
        return average;
    }

    public boolean passYear(){
        boolean pass = true;
        for (int i = 0; i < markAmount; i++) {
            if(marks[i] < 3){
                pass = false;
            }
        }
        return pass;
    }

    public int getIndexNumber() {
        return indexNumber;
    }

    public void setIndexNumber(int indexNumber) {
        this.indexNumber = indexNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getMarkAmount() {
        return markAmount;
    }

    public void setMarkAmount(int markAmount) {
        this.markAmount = markAmount;
    }
}
