package Zadanie13_StudentGradeList;

import java.util.Scanner;

public class Zadanie13Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        String inputLine = "";

        //dodac oceny
        //oblicz srednia
        //sprawdz pass

        while (!inputLine.equals("quit")){
            Student student = new Student();
            System.out.println("Choose on of the options:");
            System.out.println("add mark(number), average, check (if year passed) ");
            inputLine = skan.nextLine();
            if(inputLine.equals("quit")) break;
            else if(inputLine.equals("average")) {
                System.out.println(student.calculateMarkAverage());
            }
            else if(inputLine.equals("check")){
                System.out.println("Pass: " + student.passYear());
            }
            else {
                try {
                    String[] inputArguments = inputLine.split(" ");
                    if (inputArguments[0].equals("add")) {
                        student.addMark(Double.parseDouble(inputArguments[1]));
                    } else {
                        System.out.println("Wrong command");
                    }


                } catch (NumberFormatException nfe) {
                    System.out.println("Incorrect mark format");
                } catch (ArrayIndexOutOfBoundsException aioob) {
                    System.out.println("Not enough arguments for the function");
                }
            }
        }

    }
}
