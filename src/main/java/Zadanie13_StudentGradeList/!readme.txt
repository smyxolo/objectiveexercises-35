Zadanie 13:
    Stwórz klasę Student która posiada:
        - pole numer indeksu
        - pole imie
        - pole nazwisko
        - listę ocen
        **(- tablicę ocen (stwórz tablicę maksymalnie 100 ocen) - zadanie dodatkowe z tablicą ocen)
        - metody getterów i setterów
        - metodę do obliczania średniej
        - metodę która zwraca true jeśli żadna z ocen w liście/tablicy ocen nie jest 1 ani 2,
            oraz false w przeciwnym razie.