package Zadanie4_CalculatorClass;

public class Zadanie4Main {
    public static void main(String[] args) {
        Calculator calc = new Calculator();

        int additionresult = calc.addTwoNumbers(2,4);
        System.out.println(additionresult);

        int subtractionResult = calc.subtractTwoNumbers(9,7);
        System.out.println(subtractionResult);

        int multiplicationResult = calc.multiplyTwoNumbers(9,9);
        System.out.println(multiplicationResult);

        int divisionResult = calc.divideTwoNumbers(8,4);
        System.out.println(divisionResult);
    }
}
