package Zadanie11_BattleshipMemory;

import java.util.Scanner;

public class Zadanie11Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        Field field = new Field();

        field.newGame();
        field.printField();
        String inputLine = "";

        while (!inputLine.equals("quit")){
            System.out.println("Choose on of the following options:");
            System.out.println("check x y, print, quit");

            inputLine = skan.nextLine();
            if(inputLine.equals("quit")){
                break;
            }
            else if(inputLine.equals("print")){
                field.printField();
            }
            else if(inputLine.split(" ")[0].equals("check")){
                try {
                    field.checkCell(Integer.parseInt(inputLine.split(" ")[1]), Integer.parseInt(inputLine.split(" ")[2]));
                }
                catch (NumberFormatException nfe){
                    System.out.println("Incorrect coodinate format.");
                }
                catch (IndexOutOfBoundsException ioobe){
                    System.out.println("Not enough coordinates.");
                }
            }
        }

    }
}
